package source;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Actions {
	

	public static void main(String[] args) throws Exception {
		String cookie = login();
		String hierarchial = getTestSetFolderID("Prueba ALM report", cookie);
		String testSetId = getTestSetID("Login",hierarchial,cookie);
		//Problemas acentos, del xml se recogen mal
		Map<String, String> instance = getTestInstance("Login - Entrar sin activar recapcha - Chrome v.77 [1]",testSetId,cookie);
		String xml = updateRunXmml(cookie, instance,testSetId,"Passed","Paso 2: Error al introducir texto");
		
		System.out.print(xml);
		uploadRun(xml, cookie);
	}
	


	/**
	 * 
	 * Actions methods
	 * 
	 */

	public static String login() throws Exception {

		String jsonInputString = "{\"clientId\":\"" + Constants.CLIENTID + "\", \"secret\":\"" + Constants.SECRET + "\"}";
		return doPostLogin(Constants.LOGINURL, jsonInputString);
	}
	
	public static String getTestSetFolderID(String testsetFolderName, String cookie) throws Exception {
		
		HttpURLConnection con = doGet(Constants.TESTSETFOLDERURL+"?query={name['"+ URLEncoder.encode(testsetFolderName, "UTF-8") +"']}",cookie);
		StringBuilder responsexml = getResponsexml(con);
		
		Document doc = parseXMLtoDoc(responsexml);
		doc.getDocumentElement().normalize();
		
		XPathFactory factory = XPathFactory.newInstance();
	    NodeList nodeList = (NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"hierarchical-path\"]", doc, XPathConstants.NODESET);
	    
	    return ((Element)nodeList.item(0)).getTextContent();
	}

	public static Map <String, String> getTestInstance(String testIntanceName, String id,String cookie) throws Exception {

		HttpURLConnection con = doGet(Constants.TESTINSTANCEURL+"?query={"+URLEncoder.encode("cycle-id", "UTF-8")+"["+id+"]}",cookie);
		StringBuilder responsexml = getResponsexml(con);
		Document doc = parseXMLtoDoc(responsexml);
		doc.getDocumentElement().normalize();
		
		Map <String, String> values = new HashMap<String, String>();
		XPathFactory factory = XPathFactory.newInstance();
	    NodeList nodeList = (NodeList) (factory.newXPath()).evaluate("//Entity", doc, XPathConstants.NODESET);
	    for (int temp=0; temp<nodeList.getLength(); temp++) {
	    	NodeList nodeList1 = (NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"name\"]", nodeList.item(temp), XPathConstants.NODESET);			   
	    	if(((Element)nodeList1.item(temp)).getTextContent().equals(testIntanceName)) {
			    values.put("id", ((Element)((NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"id\"]", nodeList.item(temp), XPathConstants.NODESET)).item(temp)).getTextContent());
			    values.put("test-id",((Element)((NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"test-id\"]", nodeList.item(temp), XPathConstants.NODESET)).item(temp)).getTextContent());
			    values.put("test-config-id", ((Element)((NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"test-config-id\"]", nodeList.item(temp), XPathConstants.NODESET)).item(temp)).getTextContent());
			    return values;
			}
	    }
		return values;

	}
	
	public static String getTestSetID(String testsetName, String hierarchial,String cookie) throws Exception {

		HttpURLConnection con = doGet(Constants.TESTSETURL+"?query={test-set-folder.hierarchical-path["+ URLEncoder.encode(hierarchial, "UTF-8") +"]}",cookie);
		StringBuilder responsexml = getResponsexml(con);
		Document doc = parseXMLtoDoc(responsexml);
		doc.getDocumentElement().normalize();
		
		XPathFactory factory = XPathFactory.newInstance();
	    NodeList nodeList = (NodeList) (factory.newXPath()).evaluate("//Entity", doc, XPathConstants.NODESET);
	    for (int temp=0; temp<nodeList.getLength(); temp++) {
		   NodeList nodeList1 = (NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"name\"]", nodeList.item(temp), XPathConstants.NODESET);			   
		   if(((Element)nodeList1.item(temp)).getTextContent().equals(testsetName)) {
			   	return ((Element)((NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"id\"]", nodeList.item(temp), XPathConstants.NODESET)).item(temp)).getTextContent();
		   }
	    }
		return "";
	} 
	
	private static void uploadRun(String xml,String cookie) throws Exception {
		HttpURLConnection con = doPostCreate(Constants.TESTRUNURL, xml,cookie);
		StringBuilder responsexml = getResponsexml(con);
		Document doc = parseXMLtoDoc(responsexml);
		doc.getDocumentElement().normalize();
		
		XPathFactory factory = XPathFactory.newInstance();
	    String id = ((Element)((NodeList) (factory.newXPath()).evaluate("//Field[@Name=\"id\"]", doc, XPathConstants.NODESET)).item(0)).getTextContent();
	    doPostUpload(Constants.TESTRUNURL + "/" + id + "/attachments", xml,cookie); // A�adido el /attachments despues del idRun
	}
	




	/**
	 * 
	 * Utils
	 *  
	 */
	
	public static String updateRunXmml(String cookie, Map<String, String> instance,String cycle, String status,String error) {
		Date date = new Date();
		DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" + 
				"<Entity Type=\"run\">\r\n" + 
				"<Fields>\r\n" + 
				"<Field Name=\"test-config-id\"><Value>"+instance.get("test-config-id")+"</Value></Field>\r\n" + 
				"<Field Name=\"cycle-id\"><Value>"+cycle+"</Value></Field>\r\n" + 
				"<Field Name=\"test-id\"><Value>"+instance.get("test-id")+"</Value></Field>\r\n" + 
				"<Field Name=\"testcycl-id\"><Value>"+instance.get("id")+"</Value></Field>\r\n" + 
				"<Field Name=\"build-revision\"><Value>1</Value></Field>\r\n" + 
				"<Field Name=\"name\"><Value>MyRun</Value></Field>\r\n" + 
				"<Field Name=\"owner\"><Value>automation</Value></Field>\r\n" + 
				"<Field Name=\"comments\"><Value>"+error+"</Value></Field>\r\n" + 
				"<Field Name=\"subtype-id\"><Value>hp.qc.run.external-test</Value></Field>\r\n" + 
				"<Field Name=\"duration\"><Value>5</Value></Field>\r\n" + 
				"<Field Name=\"execution-date\"><Value>"+dateFormat.format(date)+"</Value></Field>\r\n" + 
				"<Field Name=\"execution-time\"><Value>"+hourFormat.format(date)+"</Value></Field>\r\n" + 
				"<Field Name=\"status\"><Value>Passed</Value></Field>\r\n" + 
				"</Fields>\r\n" + 
				"</Entity>");
		
		return xml.toString();
		
	}
	
	public static StringBuilder getResponsexml(HttpURLConnection con) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
		StringBuilder sb = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
		  sb.append(output);
		}
		return sb;
	}
	
	public static Document parseXMLtoDoc(StringBuilder responsexml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		ByteArrayInputStream input = new ByteArrayInputStream(responsexml.toString().getBytes("UTF-8"));
		Document doc = builder.parse(input);
		
		return doc;
	}
	
	/**
	 * 
	 * Connection methods
	 *  
	 */
	private static HttpURLConnection doGet(String url,String cookie) throws Exception {

		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/xml");
		con.setRequestProperty("Cookie", cookie);
		
		//this.con.connect();
		if (!HttpStatusCode.OK.asText().equals(Integer.toString(con.getResponseCode()))) {
			System.out.println(con.getHeaderFields());
			throw new Exception("Failed: " + Integer.toString(con.getResponseCode()) + "-" + con.getResponseMessage());
		}
		return con;

	}
	
	private static HttpURLConnection doPostUpload(String url, String xml, String cookie) throws Exception {
		
		 Path path = Paths.get("C:/IDFront.png");
	        byte[] data = Files.readAllBytes(path);
	       // System.out.println(data);
	        String octetStreamFileName = "ServerError.png";
	        
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setConnectTimeout(20000);
        con.setReadTimeout(20000);
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Slug", octetStreamFileName);
        con.setRequestProperty("Content-Type", "application/octet-stream");
        con.setRequestProperty("Cookie", cookie);
        
        OutputStream outputStream = con.getOutputStream();
        outputStream.write(data);
        outputStream.flush();
        outputStream.close();
        con.connect();
        
        if (!HttpStatusCode.CREATED.asText().equals(Integer.toString(con.getResponseCode()))) {
			System.out.println("doPostUpload: " + con.getHeaderFields());
        }
		return con;
	}
	
	public static HttpURLConnection doPostCreate(String url, String xml, String cookie) throws Exception {
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        con.setConnectTimeout(20000);
        con.setReadTimeout(20000);
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Accept", "application/xml");
        con.setRequestProperty("Content-Type", "application/xml");
        con.setRequestProperty("Cookie", cookie);
        OutputStream outputStream = con.getOutputStream();
        byte[] b = xml.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();
        con.connect();
        if (!HttpStatusCode.CREATED.asText().equals(Integer.toString(con.getResponseCode()))) {
			System.out.println(con.getHeaderFields());
        }
		return con;

	}
	
	public static void doPost(String url, String xml, String cookie) throws Exception {

		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setConnectTimeout(20000);
		con.setReadTimeout(20000);
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", "application/xml");
		con.setRequestProperty("Content-Type", "application/xml");
		con.setRequestProperty("Cookie", cookie);
		con.connect();
        
        if (!HttpStatusCode.OK.asText().equals(Integer.toString(con.getResponseCode()))) {
			System.out.println("\nHeader: " + con.getHeaderFields());			
			throw new Exception("Failed: " + Integer.toString(con.getResponseCode()) + "-" + con.getResponseMessage());
		}
	}

	public static String doPostLogin(String url, String jsonInputString) throws Exception {

		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; utf-8");

		if (!jsonInputString.isEmpty()) {
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}
		}

		con.connect();
		if (!HttpStatusCode.OK.asText().equals(Integer.toString(con.getResponseCode()))) {
			throw new Exception(
					"Login failed: " + Integer.toString(con.getResponseCode()) + "-" + con.getResponseMessage());
		}
		
		List<String> cookieList = null;
		for (Entry<String, List<String>> entry : con.getHeaderFields().entrySet()) {
			if(entry.getKey()!=null) {
				if (entry.getKey().equals("Set-Cookie")) {
					cookieList = entry.getValue();
					break;
					}
				}}

		String cookie = cookieList.get(2).split(";")[0] + ";"
				+ cookieList.get(1).split(";")[0] + ";" 
				+ cookieList.get(0).split(";")[0] + ";"
				+ cookieList.get(3).split(";")[0];
		
		System.out.println("Cookie:" + cookie);
		
		return cookie;
	}

}
