package source;

/**
*
* These constants are used throughout the code to set the server to work with.
* To execute this code, change these settings to fit those of your server.
*/
public class Constants {
	private Constants() {
	}

	/**
	 * Login 
	 */
	public static final String HOST = "192.168.33.31";
	public static final String PORT = "8080";

	public static final String USERNAME = "automation";
	public static final String PASSWORD = "4automat1on";

	public static final String DOMAIN = "DEFAULT";
	public static final String PROJECT = "A3SEC_Pruebas_Manuales";
	
	public static final String CLIENTID = "apikey-dlttetmekdsqpociitin";
	public static final String SECRET = "opafdkplhjpeelcb";
	
	public static final String LOGINURL = "http://"+HOST+":"+PORT+"/qcbin/rest/oauth2/login";
	
	/**
	 * TestSet 
	 */
	public static final String TESTSETFOLDERURL = "http://"+HOST+":"+PORT+"/qcbin/rest/domains/"+DOMAIN+"/projects/"+PROJECT+"/test-set-folders";
	public static final String TESTSETURL = "http://"+HOST+":"+PORT+"/qcbin/rest/domains/"+DOMAIN+"/projects/"+PROJECT+"/test-sets/";

	/**
	 * TestInstances
	 */
	public static final String TESTINSTANCEURL = "http://"+HOST+":"+PORT+"/qcbin/rest/domains/"+DOMAIN+"/projects/"+PROJECT+"/test-instances";
	
	/**
	 * TestRuns
	 */
	public static final String TESTRUNURL = "http://"+HOST+":"+PORT+"/qcbin/rest/domains/"+DOMAIN+"/projects/"+PROJECT+"/runs";
	
	
}

	
