package source;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Utils {
	/**
	 * 
	 * Utils
	 *  
	 */
	
	public String updateRunXmml(String cookie, Map<String, String> instance,String cycle, String status, String error) {
		Date date = new Date();
		DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" + 
				"<Entity Type=\"run\">\r\n" + 
				"<Fields>\r\n" + 
				"<Field Name=\"test-config-id\"><Value>"+instance.get("test-config-id")+"</Value></Field>\r\n" + 
				"<Field Name=\"cycle-id\"><Value>"+cycle+"</Value></Field>\r\n" + 
				"<Field Name=\"test-id\"><Value>"+instance.get("test-id")+"</Value></Field>\r\n" + 
				"<Field Name=\"testcycl-id\"><Value>"+instance.get("id")+"</Value></Field>\r\n" + 
				"<Field Name=\"build-revision\"><Value>1</Value></Field>\r\n" + 
				"<Field Name=\"name\"><Value>MyRun</Value></Field>\r\n" + 
				"<Field Name=\"owner\"><Value>automation</Value></Field>\r\n" + 
				"<Field Name=\"comments\"><Value>"+error+"</Value></Field>\r\n" + 
				"<Field Name=\"subtype-id\"><Value>hp.qc.run.external-test</Value></Field>\r\n" + 
				"<Field Name=\"duration\"><Value>5</Value></Field>\r\n" + 
				"<Field Name=\"execution-date\"><Value>"+dateFormat.format(date)+"</Value></Field>\r\n" + 
				"<Field Name=\"execution-time\"><Value>"+hourFormat.format(date)+"</Value></Field>\r\n" + 
				"<Field Name=\"status\"><Value>Passed</Value></Field>\r\n" + 
				"</Fields>\r\n" + 
				"</Entity>");
		
		return xml.toString();
		
	}
	
	public StringBuilder getResponsexml(HttpURLConnection con) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
		StringBuilder sb = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
		  sb.append(output);
		}
		return sb;
	}
	
	public Document parseXMLtoDoc(StringBuilder responsexml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		ByteArrayInputStream input = new ByteArrayInputStream(responsexml.toString().getBytes("UTF-8"));
		Document doc = builder.parse(input);
		
		return doc;
	}
}
