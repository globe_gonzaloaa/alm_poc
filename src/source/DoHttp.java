package source;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map.Entry;

public class DoHttp {
	
	
	/**
	 * 
	 * Connection methods
	 *  
	 */
	private HttpURLConnection doGet(String url,String cookie) throws Exception {

		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/xml");
		con.setRequestProperty("Cookie", cookie);
		
		//this.con.connect();
		if (!HttpStatusCode.OK.asText().equals(Integer.toString(con.getResponseCode()))) {
			System.out.println(con.getHeaderFields());
			throw new Exception("Failed: " + Integer.toString(con.getResponseCode()) + "-" + con.getResponseMessage());
		}
		return con;

	}
	
	public void doPost(String url, String xml, String cookie) throws Exception {

		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setConnectTimeout(20000);
		con.setReadTimeout(20000);
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", "application/xml");
		con.setRequestProperty("Content-Type", "application/xml; utf-8");
		con.setRequestProperty("Cookie", cookie);
		/*OutputStream os = con.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");    
		osw.write(xml);
		osw.flush();
		osw.close();
		os.close();*/
		OutputStream outputStream = new BufferedOutputStream(con.getOutputStream());
		//byte[] b = xml.getBytes("UTF-8");
		outputStream.write(xml.getBytes());
		outputStream.flush();
		outputStream.close();
        //con.connect();
        if (!HttpStatusCode.OK.asText().equals(Integer.toString(con.getResponseCode()))) {
			System.out.println(con.getHeaderFields());
			throw new Exception("Failed: " + Integer.toString(con.getResponseCode()) + "-" + con.getResponseMessage());
		}
	}

	public String doPostLogin(String url, String jsonInputString) throws Exception {

		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; utf-8");

		if (!jsonInputString.isEmpty()) {
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}
		}

		con.connect();
		if (!HttpStatusCode.OK.asText().equals(Integer.toString(con.getResponseCode()))) {
			throw new Exception(
					"Login failed: " + Integer.toString(con.getResponseCode()) + "-" + con.getResponseMessage());
		}
		
		List<String> cookieList = null;
		for (Entry<String, List<String>> entry : con.getHeaderFields().entrySet()) {
			if(entry.getKey()!=null) {
				if (entry.getKey().equals("Set-Cookie")) {
					cookieList = entry.getValue();
					break;
					}
				}}

		String cookie = cookieList.get(2).split(";")[0] + ";"
				+ cookieList.get(1).split(";")[0] + ";" 
				+ cookieList.get(0).split(";")[0] + ";"
				+ cookieList.get(3).split(";")[0];
		
		System.out.println("Cookie:" + cookie);
		
		return cookie;
	}


}
